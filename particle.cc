#include "particle.hh"
#include <iostream>

Particle::Particle() {}  // default empty constructor

Particle::~Particle() {}  // destructor

// Explicitly set everything in the constructor
Particle::Particle(int id, double m, double x, double y, double z) {
    ID = id;
    mass = m;  // set private member mass
    pos = {x, y, z};  // set private member (vector) of pos coordinates; needs -std=c++11
}

// Set members using an init list instead. For "plain old data" either an init list or
// the other method is fine, but when we start passing around objects, init list is preferred
Particle::Particle(int id, double m, std::vector<double> pos) : ID(id), mass(m), pos(pos) {}

void Particle::Print() {
    // Define our own Print() method
    // Note that the "this" from Javascript/Python is implied while in the class
    std::cout << "Particle " << ID << std::endl;
    std::cout << "  mass: " << mass << std::endl;
    std::cout << "  position: ( ";
    // There is no vector print, so we do it ourselves
    // Note we can get away with no {} if the body of the for loop is one line,
    // but this is often discouraged
    for (int i = 0; i < pos.size(); ++i)
        std::cout << pos[i] << " ";
    std::cout << ")" << std::endl;
}

void Particle::Write(std::ofstream &ofile) {
    // Essentially the same as Particle::Print(), but piped to ofile instead of cout
    ofile << "Particle " << ID << std::endl;
    ofile << "  mass: " << mass << std::endl;
    ofile << "  position: ( ";
    for (int i = 0; i < pos.size(); ++i)
        ofile << pos[i] << " ";
    ofile << ")" << std::endl;
}