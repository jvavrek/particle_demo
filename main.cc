#include <iostream>  // input/output stream
#include <fstream>  // file I/O
#include "particle.hh"  // our own particle class

int main() {  // main function, with no args for now
    std::cout << "hello world" << std::endl;  // std namespace, cout, endl, semicolon

    // Create new Particles, or actually, pointers to Particles, with * and new.
    // This allows us to pass around potentially huge objects without copying data.
    Particle *p0 = new Particle();  // using our empty constructor
    Particle *p1 = new Particle(1, 1.0e3, 1.0, 2.0);  // mxyz cons, with default z=0
    std::vector<double> pos {-1.0, -2.0, 0.0};
    Particle *p2 = new Particle(2, 2.0e3, pos);

    // Since p0 etc are pointers, we use -> instead of . to call methods
    p0->Print();  // p0 will print garbage, because we didn't give it any data
    p1->Print();
    p2->Print();

    // What if we cout the Particles?
    std::cout << p0 << std::endl;
    std::cout << p1 << std::endl;
    std::cout << p2 << std::endl;
    // Note: we could override the << operator for the Paricle class so that
    // std::cout does what our Print() method does, but this is often more trouble
    // than it's worth IMO.

    // Access Particle data outside the class using getters:
    std::cout << "p1 ID: " << p1->GetID() << std::endl;  // p1->ID won't work: ID is private
    std::cout << "p2 mass: " << p2->GetMass() << std::endl;  // p2 mass
    p2->SetMass(2*p2->GetMass());  // double the mass of p2
    std::cout << "new p2 mass: " << p2->GetMass() << std::endl;

    // Write to file
    std::ofstream outfile;
    outfile.open("output.txt");
    p0->Write(outfile);
    p1->Write(outfile);
    p2->Write(outfile);
    outfile.close();

    return 0;  // always return 0 from main
}

// notes:
// - in C/C++ words are not necessarily whitespace delimited, e.g., can do:
//      std::cout<<"hello world"<<std::endl;
// - similarly, newlines, tabs, etc, are only for readability
// - every statement (not every line) must end with a semicolon, e.g., these are OK:
//      std::cout << "A long string of text I would like to "  // note the space
//                << "break up over multiple lines in source code."
//                << std::endl;
// -    double x = 2.0; std::cout << "x = " << x << std::endl;

// - compile, then run:
//     $ g++ main.cc -o main  // g++ or clang; main.cc input, output to main executable
// - executable is binary and will be platform-dependent
// - can add compile flags as well, e.g.
//     $ g++ main.cc -o main -O2  // levels of runtime optimization: -O0, -O1, -O2, -O3
// - usually not a lot of runtime difference between -O2 and -O3, but can latter can
//   increase compile times; other flags for size optimization or using debuggers

// - adding the Particle class
//    - typically, compiler will search for .hh header files
//    - but we need to provide it the .cc implementation files
//     $ g++ main.cc particle.cc -o main
//    - also, vector initialization is clunky and was only impl in newer C++:
//     $ g++ main.cc particle.cc -o main -std=c++11
//    - often create a makefile which holds all compilation instructions, then `make`
