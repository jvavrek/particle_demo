#include <vector>
#include <fstream>

class Particle
{
public:  // accessible from outside the class
    // declaring class methods in .hh, implementing in .cc
    Particle();  // default empty constructor
    Particle(int id, double m, double x, double y, double z=0);  // alt cons, with names, z=0
    // Note: all args are ordered; Python-type kwargs do not exist! So defaults at end
    Particle(int, double, std::vector<double>); // alt constructor, no names
    ~Particle();  // destructor, if we allocate our own memory; no args

    // now declare our own methods; require type specifications
    void Print();  // void-type does stuff but doesn't return anything

    // getters and setters
    // Note that setters have void type, while getters have non-void type
    // Note also that we can implement the methods in the header! This is often
    // done for short methods like getters/setters.
    // Note also the 'double' semicolons
    void SetID(int id) {ID = id;};
    int GetID() {return ID;};

    void SetMass(double m) {mass = m;};
    double GetMass() {return mass;};

    void SetPos(std::vector<double> p) {pos = p;};
    std::vector<double> GetPos() {return pos;};

    // write to file; need to pass a reference to the ofstream object
    void Write(std::ofstream &ofile);

private:  // accessible only within the class
    // Typically data is made _private_, and we write _public_ Get/Set methods
    int ID;  // (signed) int type
    double mass;  // 64-bit double precision floating point
    std::vector<double> pos;  // std::vector is the go-to array; other containers
};